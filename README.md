RPGキャラクターレベルアップシミュレーション（サンプルプログラム）
====

## 概要
プログラミング応用長期課題（河野担当分）のサンプルプログラムです。  
Enum（列挙型）を使う場合のコードです。

## プログラム説明
* character : キャラクタ関連のクラス群
  -  CharacterMaking.java : キャラクタ作成の入力処理、レベルアップシミュレーション
  -  GameCharacter.java : キャラクタのベースとなる抽象クラス
  -  Hero.java : GameCharacterクラスを継承した勇者クラス
  -  Job.java : 職業のデータ構造を定義するクラス
  -  Sex.java : 性別のデータ構造を定義するクラス
  -  Warrior.java : GameCharacterクラスを継承した戦士クラス
  -  Wizard.java : GameCharacterクラスを継承した魔法使いクラス
* gameplay : ゲーム処理を行うメインクラス群
  - CharacterSimulator.java : JSON形式のファイルを読み込むサンプルプログラム
  - CharacterSimulator_chara.java : キャラクタ作成を行うサンプルプログラム
* user.json : JSON形式のサンプルプログラム 

## gitでcloneする場合
`git clone https://kawanolab-git.tuis.ac.jp/ykawano/game-character-simulator.git`

## 課題説明ページ
[プログラミング応用演習b 2018年度 長期課題（2）](http://kawano-lab.tuis.ac.jp/index.php/11-lectures/66-progab2018-longterm-kadai2)