package character;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CharacterMaking {

	// キャラクター作成時のパラメータ設定
	private static GameCharacter RandomParameter(String name, Sex sex, Job job) {
		GameCharacter chara=null;
		switch (job.JobNumber()) {
		case 1:
			chara = new Hero(name, sex, Job.Hero, Job.Hero.hp, Job.Hero.mp,
					Job.Hero.power, Job.Hero.speed, Job.Hero.guard,
					Job.Hero.intelligent, Job.Hero.luck);
			chara.levelUp();
			break;
		case 2:
			chara = new Warrior(name, sex, Job.Warrior, Job.Warrior.hp, Job.Warrior.mp,
					Job.Warrior.power, Job.Warrior.speed, Job.Warrior.guard,
					Job.Warrior.intelligent, Job.Warrior.luck);
			chara.levelUp();
			break;
		case 3:
			chara = new Wizard(name, sex, Job.Wizard, Job.Wizard.hp, Job.Wizard.mp,
					Job.Wizard.power, Job.Wizard.speed, Job.Wizard.guard,
					Job.Wizard.intelligent, Job.Wizard.luck);
			chara.levelUp();
			break;
		}
		return chara;
	}

	// キャラクター作成メソッド
	public static GameCharacter making () {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String name="";
		String buf="";

		// キャラクター情報
		GameCharacter chara=null;
		Sex sex;
		Job job=null;

	    System.out.println("なまえをいれてください。");
	    try {
			name = new String(in.readLine());
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	    do {
		    System.out.println("つぎに、せいべつをおしえてください。\n男:1, 女:2");
		    try {
				buf = new String(in.readLine());
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		    int num = Integer.parseInt(buf);
		    if (num == Sex.Man.GenderNumber() ) {
		    	sex = Sex.Man;
		    	break;
		    }
		    else if (num == Sex.Woman.GenderNumber()) {
		    	sex = Sex.Woman;
		    	break;
		    }
	    } while (true);

	    do {
		    System.out.println("職業を選択してください。");
		    for (Job j1: Job.values()) {
		    	System.out.print(j1.JobNumber()+":"+j1.JobName()+"  ");
		    }
		    System.out.println("\n");
		    try {
				buf = new String(in.readLine());
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		    // 職業の入力
		    int job_number = Integer.parseInt(buf);
		    for (Job j1: Job.values()) {
		    	if (j1.JobNumber() == job_number) {
		    		job=j1;
		    		chara = CharacterMaking.RandomParameter(name, sex, job);
					//chara.displayStatus();
		    	}
		    }

	    } while (job == null);
	    return chara;
	}

	public static void levelSimulation (GameCharacter chara) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String buf="";
		int level=0;
		do {
			System.out.println("何レベルまで成長させますか？");
			try {
				buf = new String(in.readLine());
				level = Integer.parseInt(buf) - chara.level;
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			if (level >= 1 && level <= 99) break;
		} while(true);

		for (int i=1; i<=level; i++) {
			chara.levelUp();
		}
		return;
	}
}
