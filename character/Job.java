package character;

public enum Job {
	// 勇者, 戦士, 魔法使い
	// 職業名, 職業ID,
	// パラメータ初期値（Lv 0）：HP, MP, 攻, 守
	// 力, 早, 身, 賢, 運
	Hero ("ゆうしゃ", 1,		// 職業名, 職業ID,
			18, 5,		// パラメータ初期値（Lv 0）：HP, MP, 攻, 守
			12, 8, 10, 8, 6),	// 力, 早, 身, 賢, 運
	Warrior ("せんし", 2,
			20, 0,
			14, 5, 10, 4, 5),
	Wizard ("まほうつかい", 3,
			12, 8,
			6, 9, 6, 10, 10);

	private final String name;
	private final int number;
	public final int hp, mp,
			power, speed, guard, intelligent, luck;

	private Job(String name, int number, int hp, int mp,
			int power, int speed, int guard, int intelligent, int luck) {
		this.name = name;
		this.number = number;
		this.hp = hp; this.mp = mp;
		this.power = power; this.speed = speed; this.guard = guard;
		this.intelligent = intelligent; this.luck = luck;
	}

	private Job(String name, int number) {
		this.name = name;
		this.number = number;
		this.hp = 10; this.mp = 0;
		this.power = 7; this.speed = 5; this.guard = 7;
		this.intelligent = 5; this.luck = 5;
	}

	public String JobName() {
		return name;
	}
	public int JobNumber() {
		return number;
	}

}
