package character;

public enum Sex {
	//おとこ, おんな
	Man ("おとこ", 1),
	Woman ("おんな", 2);
	
	private final String name;
	private final int number;
	
	private Sex(String name, int number) {
		this.name = name;
		this.number = number;
	}
	
	public String GenderName() {
		return name;
	}
	public int GenderNumber() {
		return number;
	}
}
