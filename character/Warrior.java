package character;

// 戦士のクラス
public class Warrior extends GameCharacter {

	// パラメータ成長値の設定
	static final double hp_up = 4.0, mp_up = 0.0,
			power_up = 2.3, speed_up = 0.6, guard_up = 1.6,
			intelligent_up = 0.6, luck_up = 1.0;
	static final double range = 2.4;

	public Warrior(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public Warrior(String name, Sex sex) {
		super (name, sex, Job.Warrior, Job.Warrior.hp, Job.Warrior.mp,
				Job.Warrior.power, Job.Warrior.speed, Job.Warrior.guard,
				Job.Warrior.intelligent, Job.Warrior.luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
	}


	@Override
	public void levelUp() {
		// TODO 自動生成されたメソッド・スタブ
		// パラメータ上昇
		this.MaxHp += (int)(Math.random()*range+hp_up);
		//this.MaxMP += (int)(Math.random()*range+mp_up); // MPは増えない
		int power_plus = (int)(Math.random()*range+power_up);
		this.power += power_plus;
		this.speed += (int)(Math.random()*range+speed_up);
		int guard_plus = (int)(Math.random()*range+guard_up);
		this.guard += guard_plus;
		this.intelligent += (int)(Math.random()*range+intelligent_up);
		this.luck += (int)(Math.random()*range+luck_up);
		this.level++;

		// HP, MP全回復
		this.hp = this.MaxHp; this.mp = this.MaxMP;
	}

}
