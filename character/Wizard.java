package character;

// 魔法使いのクラス
public class Wizard extends GameCharacter {

	// パラメータ成長値の設定
	static final double hp_up = 2.5, mp_up = 3.7,
			power_up = 0.7, speed_up = 1.4, guard_up = 0.9,
			intelligent_up = 2.0, luck_up = 1.8;
	static final double range = 2.5;

	public Wizard(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public Wizard(String name, Sex sex) {
		super (name, sex, Job.Wizard, Job.Wizard.hp, Job.Wizard.mp,
				Job.Wizard.power, Job.Wizard.speed, Job.Wizard.guard,
				Job.Wizard.intelligent, Job.Wizard.luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
	}


}
