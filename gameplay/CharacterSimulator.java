package gameplay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

class User {
	public int id;
    public String name;
    public int age;

    // 基本情報のコンストラクタ
    User(int id, String name, int age) {
    	this.id = id;
    	this.name = name;
    	this.age = age;
    }
}



public class CharacterSimulator{
	final static int MAX_USER = 10; // Userの最大人数

	public static void main(String[] args) throws FileNotFoundException,IOException {

		// User型オブジェクトの配列を宣言
		User[] users = new User[MAX_USER];

		try {
			// ObjectMapperでJSONファイルの木構造を作成
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(new File("src/user.json"));

			int i=0;
			while (root.hasNonNull(i)) { // Nullでない限り繰り返す
				// JSON形式のファイルから各パラメータを取得
				int id = root.get(i).get("id").asInt();
				String name = root.get(i).get("name").asText();
				int age = root.get(i).get("age").asInt();

				// コンストラクタでUserオブジェクトを作成
	          	users[i] = new User(id, name, age);

				System.out.println("ID: "+users[i].id+", name: "+users[i].name+", "
						+ "age: "+users[i].age);
				i++;
			}
		} catch (FileNotFoundException fe) {
			//System.out.println(fe.getMessage());
			fe.printStackTrace();
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
