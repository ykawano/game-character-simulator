package gameplay;

import java.io.IOException;

import character.CharacterMaking;
import character.GameCharacter;


public class CharacterSimulator_chara{

	public static void main(String[] args) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
/*
        ObjectMapper mapper = new ObjectMapper();
        try {
            User[] users = mapper.readValue(new File("src/user2.json"), User[].class);
            for (int i=0; i < users.length; i++) {
            	System.out.println("ID: "+users[i].id+", name:"+users[i].name
            			+ ", age:" + users[i].age);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

		// キャラクター作成
		GameCharacter chara = CharacterMaking.making();
		chara.displayStatus();

		// キャラクター成長シミュレーション
		CharacterMaking.levelSimulation(chara);
		chara.displayStatus();

	}



}
